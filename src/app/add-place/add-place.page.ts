import { Component } from '@angular/core';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder/ngx';
import { Place } from '../../models/place.model';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PlacesService } from '../../services/places.service';
import { Router } from '@angular/router';
import { WebView } from '@ionic-native/ionic-webview/ngx';

@Component({
  selector: 'app-add-place',
  templateUrl: './add-place.page.html',
  styleUrls: ['./add-place.page.scss'],
})
export class AddPlacePage {
  newPlace: Place = new Place();
  constructor(private nativeGeocoder: NativeGeocoder, 
    private geolocation: Geolocation, 
    private camera: Camera,
    private placeService: PlacesService,
    public router: Router,
    private webView: WebView) {}

  getPhoto() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.ALLMEDIA,
      correctOrientation: true
    }
     this.camera.getPicture(options).then((imageData) => {
      this.newPlace.picture = this.webView.convertFileSrc(imageData);
     }, (err) => {
        console.log(err)
     });
  }

  getAddressInfo() {
    let options: NativeGeocoderOptions = {
      useLocale: true,
      maxResults: 5
    };
    this.geolocation.getCurrentPosition().then((resp) => {
      this.nativeGeocoder.reverseGeocode(resp.coords.latitude, resp.coords.longitude, options).then((result: NativeGeocoderReverseResult[]) => {
          let addressInfo:NativeGeocoderReverseResult  = result[0];
          this.newPlace.address = addressInfo.thoroughfare + ' ' + addressInfo.subThoroughfare + ', ' + addressInfo.postalCode + ' ' + addressInfo.subAdministrativeArea;
        }).catch((error: any) => 
        console.log(error)
      );
    }).catch((error) => {
      console.log('Error getting location', error);
    });
  }

  savePlace() {
    this.placeService.addPlace(this.newPlace);
    this.router.navigateByUrl('/home');
  }
}
