import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { Place } from '../../models/place.model';
import { PlacesService } from '../../services/places.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  places: Place[] = new Array();
  constructor(private router: Router, private placesService: PlacesService){
  }

  ngOnInit(): void {
  }

  ionViewWillEnter() {
    this.places = this.placesService.getPlaces();
  }

  deletePlace(id: number) {
      this.places.splice(id, 1);
      this.placesService.deletePlace(id);
  }

  goToAddPlacePage() {
    this.router.navigateByUrl('/addPlace');
  }
}
