import { Injectable } from '@angular/core';
import { Place } from '../models/place.model';

@Injectable({
  providedIn: 'root'
})
export class PlacesService {
  constructor() { }

  getPlaces(): Place[]{
    return JSON.parse(localStorage.getItem('Places'));
  }

  setPlaces(places: Place[]) {
    localStorage.setItem('Places', JSON.stringify(places));
  }

  addPlace(place: Place){
    let places: Place[] = new Array();
    if(this.getPlaces() != null) {
     places = this.getPlaces();
    }
    places.push(place);
    this.setPlaces(places);
  }

  deletePlace(index: number) {
    let places: Place[] = new Array();
    places = this.getPlaces();
    places.splice(index, 1);
    this.setPlaces(places);
  }

}
