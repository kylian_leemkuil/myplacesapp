export class Place {
    public title: string;
    public description: string;
    public picture: any;
    public address: string;

    constructor(title?: string, description?: string, picture?: any, address?: string){
        this.title = title;
        this.description = description;
        this.picture = picture;
        this.address = address;
    }
}
