# MyPlacesApp

## Clone repository
```sh
git clone https://kylian_leemkuil@bitbucket.org/kylian_leemkuil/myplacesapp.git
```

## Development android
1. Run the command below
```sh 
npm install
```
2. Run the command below for android development
```sh 
ionic cordova run android
```
3. The app will now debug on your device or emulator


## Development iOS
1. Run the command below
```sh 
npm install
```
2. Run the command below for android development
```sh 
ionic cordova run ios
```
3. Open the project in Xcode.
4. Oopen Xcode. Use File » Open and locate the app. Open the app's "platforms/ios" directory.
5. In Xcode, select a target simulator or device and click the play button.
6. The app will now debug on your emulator